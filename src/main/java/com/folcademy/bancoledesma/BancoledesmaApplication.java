package com.folcademy.bancoledesma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoledesmaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoledesmaApplication.class, args);
	}

}
