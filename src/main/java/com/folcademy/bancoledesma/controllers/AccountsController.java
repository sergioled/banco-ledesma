package com.folcademy.bancoledesma.controllers;

import com.folcademy.bancoledesma.models.entities.AccountEntity;
import com.folcademy.bancoledesma.models.repositories.AccountRepositories;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountsController {
    private final AccountRepositories accountRepositories;

    public AccountsController( AccountRepositories accountRepositories ) {
        this.accountRepositories = accountRepositories;
    }

    @GetMapping("{user}")
    public List<AccountEntity> getUsersAccounts(@PathVariable String user){
        return accountRepositories.findAllByUser(user);
    }

    @GetMapping("/allUser")
    public List<AccountEntity> getAllUserAccounts(){
        return (List<AccountEntity>) accountRepositories.findAll();
    }

    @PostMapping("/new")
    public  AccountEntity newAccount(@RequestBody AccountEntity accountEntity){
        return  accountRepositories.save(accountEntity);
    }
}
