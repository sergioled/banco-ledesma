package com.folcademy.bancoledesma.models.enums;

public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORRO
}
