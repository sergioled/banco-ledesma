package com.folcademy.bancoledesma.models.repositories;

import com.folcademy.bancoledesma.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepositories extends CrudRepository<AccountEntity,Long> {
    List<AccountEntity> findAllByUser(String user);
}
