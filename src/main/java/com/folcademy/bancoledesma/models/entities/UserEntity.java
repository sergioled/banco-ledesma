package com.folcademy.bancoledesma.models.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "users")
public class UserEntity {
    @Id
    private String dni;
    private String firstname;
    private String lastName;
    private String adress;

    public UserEntity(){

    }

    public String getDni(){
        return dni;
    }
    public String getFirstname(){
        return firstname;
    }
    public String getLastName(){
        return lastName;
    }
    public String getAdress(){
        return adress;
    }
}
