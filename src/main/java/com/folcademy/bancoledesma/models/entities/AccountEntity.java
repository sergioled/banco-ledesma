package com.folcademy.bancoledesma.models.entities;

import com.folcademy.bancoledesma.models.enums.AccountType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity(name = "accounts")
public class AccountEntity {
    @Id
    private Long number;
    private String cbu;
    @Enumerated(EnumType.STRING)
    private AccountType type;
    private  String user;
    @SuppressWarnings("unused")
    public  AccountEntity(){

    }
    @SuppressWarnings("unused")
    public AccountEntity(Long number, String cbu, AccountType type, String user){
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.user = user;
    }
    public Long getNumber(){
        return number;
    }
    public String getCbu(){
        return cbu;
    }
    public AccountType getType(){
        return type;
    }
    public String getUser(){
        return user;
    }
}
